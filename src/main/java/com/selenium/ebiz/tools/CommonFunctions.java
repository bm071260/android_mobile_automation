package com.selenium.ebiz.tools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.selenium.ebiz.pageobject.PageObjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public class CommonFunctions{

	AppiumDriver<WebElement> driver;
	
    public CommonFunctions(AppiumDriver<WebElement> driver2) {
        this.driver = driver2;
    }

     
	
	public void scrollandClickElement(AppiumDriver<WebElement> driver, WebElement element) {
		waitForElement(driver, element, 60);
		element.click();	
	}
	
	public void scrollToElement(AppiumDriver<WebElement> driver, String controlText) {
		driver.findElement(MobileBy.AndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+ controlText + "\").instance(0))"));
	
	}
	

	public boolean waitForElement(AppiumDriver<WebElement> driver, int timeLimitInSeconds, WebElement element, String controlID){
	boolean isElementPresent;
	try{
		//mobileElement = driver.findElement(MobileBy.AndroidUIAutomator("new UiSelector().textContains(\""+controlText+"\").instance(0)"));
		WebDriverWait wait = new WebDriverWait(driver, timeLimitInSeconds);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(controlID))); 
		isElementPresent = element.isDisplayed();
		return isElementPresent;	
	}catch(Exception e){
		isElementPresent = false;
		System.out.println(e.getMessage());
		return isElementPresent;
	} }
	
	public boolean waitForElement(AppiumDriver<WebElement> driver, int timeLimitInSeconds, String controlText){
		boolean isElementPresent;
		WebElement mobileElement;
		try{
			mobileElement = driver.findElement(MobileBy.AndroidUIAutomator("new UiSelector().textContains(\""+controlText+"\").instance(0)"));
			WebDriverWait wait = new WebDriverWait(driver, timeLimitInSeconds);
			wait.until(ExpectedConditions.visibilityOf(mobileElement)); 
			isElementPresent = mobileElement.isDisplayed();
			return isElementPresent;	
		}catch(Exception e){
			isElementPresent = false;
			System.out.println(e.getMessage());
			return isElementPresent;
		} }
	
	public boolean waitForElement(AppiumDriver<WebElement> driver, WebElement element, int timeLimitInSeconds){
		boolean isElementPresent;
		try{
			//mobileElement = driver.findElement(MobileBy.AndroidUIAutomator("new UiSelector().textContains(\""+controlText+"\").instance(0)"));
			WebDriverWait wait = new WebDriverWait(driver, timeLimitInSeconds);
			wait.until(ExpectedConditions.visibilityOf(element)); 
			isElementPresent = element.isDisplayed();
			return isElementPresent;	
		}catch(Exception e){
			isElementPresent = false;
			System.out.println(e.getMessage());
			return isElementPresent;
		} }


	

}
