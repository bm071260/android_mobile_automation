package com.selenium.ebiz.tools;

import com.selenium.ebiz.pageobject.*;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageManager {

    private AppiumDriver<WebElement> driver;
    private LoginPage loginPage;
    private com.selenium.ebiz.pageobject.HomePage homePage;

    public PageManager(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
    }

    public LoginPage loginPage(){
        return new LoginPage((AppiumDriver<WebElement>) driver);
    }

    public HomePage homePage(){
        return new HomePage((AppiumDriver<WebElement>) driver);
    }
    
    public ShoppingCartPage shoppingCartPage(){
        return new ShoppingCartPage((AppiumDriver<WebElement>) driver);
    }

    public CommonFunctions commonFunctions(){
        return new CommonFunctions((AppiumDriver<WebElement>) driver);
    }


}



