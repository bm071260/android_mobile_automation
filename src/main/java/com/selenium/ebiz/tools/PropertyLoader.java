package com.selenium.ebiz.tools;

import java.io.IOException;
import java.util.Properties;
/**
 * Loads test suite configuration from resource files.
 */
public class PropertyLoader {

    private static final String DEBUG_PROPERTIES = "config/debug.properties";

    private Properties prop;


    public PropertyLoader() throws IOException {
    
          this (System.getProperty("environment.properties", DEBUG_PROPERTIES));
    }


    private  PropertyLoader(String fromResource) throws IOException {
    //	System.out.println(fromResource);
        prop = new Properties();
        prop.load(PropertyLoader.class.getResourceAsStream(fromResource));
    }

    public boolean hasProperty(String name) {
        return prop.containsKey(name);
    }

    public String getProperty(String name) {
        return prop.getProperty(name);
    }


}