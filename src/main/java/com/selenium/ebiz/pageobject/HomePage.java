package com.selenium.ebiz.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.selenium.ebiz.tools.PageManager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomePage extends PageObjects{

	private PageManager pageManager;

	public HomePage(AppiumDriver<WebElement> driver)
	{
		super(driver);
		this.driver = driver;

	}

	@AndroidFindBy(className = "android.widget.TextView")
	public WebElement searchField;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.ImageView")
	public WebElement scanIcon;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView")
	public WebElement searchButton;
	
	public WebElement addToCart = driver.findElement(MobileBy.AndroidUIAutomator(
			"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"ADD TO CART\").instance(0))"));

	/*MobileElement addToCart = (MobileElement)driver2.findElement(MobileBy.AndroidUIAutomator(
			"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"ADD TO CART\").instance(0))"));
	 */

	@AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc='Cart, tab, 3 of 5']/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView")
	public WebElement cartIcon ;

	/*@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[4]/android.view.View/android.view.View[3]/android.view.View[2]/android.widget.Button[3]")
	public WebElement checkout ;*/

	@AndroidFindBy(id = "poNumber")
	public WebElement poNumber;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[4]/android.view.View/android.view.View/android.view.View/android.view.View[3]/android.widget.Button[3]")
	public WebElement submitOrderButton;

	@AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc='Shop, tab, 2 of 5']/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView")
	public WebElement shopIcon;

	public void enterSearchString(String keyword){
		searchField.sendKeys(keyword);
		System.out.println("Keyword Entered");
	}

	public void clickSearchButton(){
		searchButton.click();
		System.out.println("Search button clicked");
	}

	public Boolean isDisplayedScanIcon() throws Exception{
		try{
			waitForElement(driver, scanIcon, 60);
			System.out.println("Scan Icon Displayed");
			return scanIcon.isDisplayed();
		}catch(Exception e){
			System.out.println("Scan Icon is not Displayed");
			return false;
		}
	}

	public void dupclickAddToCartButton() throws Exception{
		pageManager.commonFunctions().waitForElement(driver, scanIcon, 60);
		//addToCart.click();
		//Thread.sleep(3000);

	}

	public void clickAddToCartButton() throws Exception{
		try{System.out.println("Enter Adde to click addtocart button");
		//pageManager.commonFunctions().waitForElement(driver, addToCart, 60);
		//pageManager.commonFunctions().scrollandClickElement(driver, "ADD TO CART");
		scrollandClickElement(driver, "ADD TO CART");
		//Thread.sleep(3000);
		}catch(Exception e){
			System.out.println("Add To Cart is not Displayed. "+ e.getMessage().toString());
			
		}
	}
	
	public void scrollandClickElement(AppiumDriver<WebElement> driver, String controlText) {
   	 try{
   		 System.out.println("Enter scroll and Click element");
		driver.findElement(MobileBy.AndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+ controlText + "\").instance(0))")).click();
		//element.click();
   	 }catch(Exception e){
			System.out.println("Unable to scroll and click: " + controlText + e.getMessage().toString());
		}
	
	}
	
	public boolean waitForElement(AppiumDriver<WebElement> driver, WebElement element, int timeLimitInSeconds){
		boolean isElementPresent;
		try{
			//mobileElement = driver.findElement(MobileBy.AndroidUIAutomator("new UiSelector().textContains(\""+controlText+"\").instance(0)"));
			WebDriverWait wait = new WebDriverWait(driver, timeLimitInSeconds);
			wait.until(ExpectedConditions.visibilityOf(element)); 
			isElementPresent = element.isDisplayed();
			return isElementPresent;	
		}catch(Exception e){
			isElementPresent = false;
			System.out.println(e.getMessage());
			return isElementPresent;
		} }

	public void clickCartIcon(){
		cartIcon.click();
		System.out.println("Cart Icon clicked");
	}

	public void clickShopIcon(){
		shopIcon.click();
		System.out.println("Shop Icon clicked");
	}

	public void enterPONumber(String ponum) throws Exception{
		Thread.sleep(10000);
		scrollToElement(driver, "P.O. Number");
		driver.findElement(MobileBy.AndroidUIAutomator(
				"new UiSelector().resourceId(\"poNumber\")")).sendKeys(ponum);
		System.out.println("Search button clicked");
	}
	
	public void scrollToElement(AppiumDriver<WebElement> driver, String controlText) {
		driver.findElement(MobileBy.AndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+ controlText + "\").instance(0))"));
	
	}

	public void clickSubmitOrderButton() throws Exception{
	//	pageManager.commonFunctions().scrollandClickElement((AppiumDriver<WebElement>) driver, "SUBMIT SECURE ORDER");
		scrollandClickElement( driver, "SUBMIT SECURE ORDER");
		System.out.println("SubmitOrderButton button clicked");
		Thread.sleep(15000);
	}
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[4]/android.view.View/android.view.View[3]/android.view.View[2]/android.view.View[2]")
	public WebElement shoppingCartHeadingText ;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[4]/android.view.View/android.view.View[3]/android.view.View[2]/android.widget.Button[3]")
	public WebElement checkout ;

	public Boolean isDisplayedShoppingCartHeadingText() throws Exception{
		Thread.sleep(20000);
		Boolean isDisplayed =  driver.findElement(MobileBy.AndroidUIAutomator(String.format("new UiSelector().text(\"%s\")", "SHOPPING CART"))).isDisplayed();
		return isDisplayed;
	}

	public void clickCheckoutButton(){
		//pageManager.commonFunctions().scrollandClickElement((AppiumDriver<WebElement>) driver, "CHECKOUT");
		scrollandClickElement(driver, "CHECKOUT");
	}

	public void addPartToCartAndSubmitOrder() throws Exception{
		isDisplayedScanIcon();
		//System.out.println("Scan Icon is Displayed");

		clickAddToCartButton();
		// dupclickAddToCartButton();
		//System.out.println("Clicked on Add To Cart Button");

		clickCartIcon();
		//System.out.println("Clicked on Shopping cart Icon");

		Boolean isDisplayedshoppingCart =isDisplayedShoppingCartHeadingText();
		//System.out.println("Shopping Cart Heading Displayed");

		clickCheckoutButton();
		//System.out.println("Clicked on Checkout Button");

		enterPONumber("1234");  
		//System.out.println("Entered PO number");

		clickSubmitOrderButton();
		//System.out.println("Clicked on Submit Order Button");

		Assert.assertTrue(isDisplayedshoppingCart);
	}

}