package com.selenium.ebiz.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PageObjects {

	AppiumDriver<WebElement> driver;

    public PageObjects(AppiumDriver<WebElement> driver2) {

        this.driver = driver2;
        PageFactory.initElements(new AppiumFieldDecorator(driver2), this);

    }
}
