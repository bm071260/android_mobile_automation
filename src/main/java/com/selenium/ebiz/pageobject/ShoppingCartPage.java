package com.selenium.ebiz.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.selenium.ebiz.tools.PageManager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ShoppingCartPage extends PageObjects{

	WebDriver driver;

	private PageManager pageManager;

	public ShoppingCartPage(AppiumDriver<WebElement> driver)
	{
		super(driver);
		this.driver = driver;
	}

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[4]/android.view.View/android.view.View[3]/android.view.View[2]/android.view.View[2]")
	public WebElement shoppingCartHeadingText ;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View[4]/android.view.View/android.view.View[3]/android.view.View[2]/android.widget.Button[3]")
	public WebElement checkout ;

	public Boolean isDisplayedShoppingCartHeadingText() throws Exception{
		Thread.sleep(20000);
		Boolean isDisplayed =  driver.findElement(MobileBy.AndroidUIAutomator(String.format("new UiSelector().text(\"%s\")", "SHOPPING CART"))).isDisplayed();
		return isDisplayed;
	}

	public void clickCheckoutButton(){
		//pageManager.commonFunctions().scrollandClickElement((AppiumDriver<WebElement>) driver, "CHECKOUT");
		pageManager.homePage().scrollandClickElement((AppiumDriver<WebElement>) driver, "CHECKOUT");
	}


}