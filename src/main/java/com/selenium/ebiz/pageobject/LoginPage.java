package com.selenium.ebiz.pageobject;

import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class LoginPage extends PageObjects{

	public LoginPage(AppiumDriver<WebElement> driver)
	{
		super(driver);
		this.driver = driver;
	}

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.EditText")
	public WebElement userNameTxt;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.EditText")
	public WebElement passwordTxt;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.Button")
	public WebElement signInButton;

	public void enterUserName(String username){
		userNameTxt.sendKeys(username);
		System.out.println("UserName Entered");
	}

	public void enterPassword(String password){
		passwordTxt.sendKeys(password);
		System.out.println("Password Entered");
	}

	public void clickSigInButton() throws Exception{
		signInButton.click();
		System.out.println("Clicked on SigIn Button");
		
	}
	
	public void signInToHDS(String userName,String password) throws Exception{
		enterUserName(userName);
		enterPassword(password);
		clickSigInButton();
		
	}

}