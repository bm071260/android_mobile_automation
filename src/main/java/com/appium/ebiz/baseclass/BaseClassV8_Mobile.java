package com.appium.ebiz.baseclass;
import com.saucelabs.common.SauceOnDemandAuthentication;
import io.appium.java_client.remote.MobileCapabilityType;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.log4j.Logger;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
import com.selenium.ebiz.tools.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseClassV8_Mobile extends SauceOnDemandAuthentication {
	protected PropertyLoader propertyRead;
	protected String baseUrl;
	public Logger Log = Logger.getLogger(BaseClassV8_Mobile.class.getName());

	//public static final String USERNAME = System.getenv("SAUCE_USERNAME");
	//public static final String ACCESS_KEY = System.getenv("SAUCE_ACCESS_KEY");
	//public static final String APP_PATH = System.getenv("APP_PATH");
	protected ConfigFileRead configread;

	public static final String SAUCE_USERNAME = "Bhavani.Manthri";
	public static final String SAUCE_ACCESS_KEY = "e81afb24-cbab-47e9-9fde-669f568e5dfc";
	public static final String APP_PATH = "/Users/bm071260/Documents/universal.apk";

	private SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(SAUCE_USERNAME, SAUCE_ACCESS_KEY);

	private ThreadLocal<AppiumDriver<WebElement>> webDriver = new ThreadLocal<>();

	protected ThreadLocal<String> sessionID = new ThreadLocal<>();

	/**
	 * @return the {@link Driver} for the current thread
	 */
	public WebDriver getWebDriver() {
		return webDriver.get();
	}

	/**
	 * ThreadLocal variable which contains the Sauce Job Id.
	 */

	public String getSessionID() {
		return sessionID.get();
	}

	public SauceOnDemandAuthentication getAuthentication() {
		return authentication;
	}

	//@BeforeMethod
	protected void createDriver(String platformName, String platformVersion,String deviceName,String udid) throws IOException, InterruptedException {

		/*String command = "curl -u " + USERNAME + ":" + ACCESS_KEY + " -X POST -H \"Content-Type: application/octet-stream\" " +
            "https://saucelabs.com/rest/v1/storage/" + USERNAME + "/ReactSampleApp.apk?overwrite=true --data-binary @" + APP_PATH;
    String[] args = new String[]{"/bin/bash", "-c", command, "with", "args"};
    Process uploadApp = new ProcessBuilder(command).start(); */


		configread = new ConfigFileRead();

		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,platformName );
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,platformVersion ); 
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60");
		capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"UiAutomator2");
		capabilities.setCapability("browserName", "");
		
		if (configread.getTestEnv().equalsIgnoreCase("remote")) {
			Log.info("Welcome to Remote Execution in Saucelab");

			capabilities.setCapability("username", SAUCE_USERNAME);
			capabilities.setCapability("accessKey", SAUCE_ACCESS_KEY);
			capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.13.0");
			capabilities.setCapability("deviceOrientation", "portrait");
			capabilities.setCapability("extendedDebugging", true);
			capabilities.setCapability(MobileCapabilityType.APP,"sauce-storage:universal.apk");
			
			try {
				webDriver.set(new AndroidDriver<WebElement>(new URL("https://" + SAUCE_USERNAME + ":" + SAUCE_ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub"),capabilities));
				Thread.sleep(10000);
			}catch (MalformedURLException e) {
				e.printStackTrace();
			}

			String id = ((RemoteWebDriver) getWebDriver()).getSessionId().toString();
			sessionID.set(id);
			
		}
		else if (configread.getTestEnv().equalsIgnoreCase("local")) {
			Log.info("Welcome to Execution in Local Grid");
			capabilities.setCapability(MobileCapabilityType.UDID, udid);
			capabilities.setCapability(MobileCapabilityType.APP,APP_PATH);
			webDriver.set(new AndroidDriver<WebElement>(new URL("http://localhost:4723/wd/hub"),capabilities));
			
		}
		webDriver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	/**
	 *  this method starts the appium  server depending on your OS.
	 * @param os your machine OS (windows/mac)
	 * @throws IOException Signals that an I/O exception of some sort has occurred
	 * @throws ExecuteException An exception indicating that the executing a subprocesses failed
	 * @throws InterruptedException Thrown when a thread is waiting, sleeping,
	 * or otherwise occupied, and the thread is interrupted, either before
	 *  or during the activity.
	 */
	//@DataProvider(name = "browsers",parallel = true)
	@BeforeSuite
	public void startAppiumServer() throws ExecuteException, IOException, InterruptedException{
		CommandLine cmd = new CommandLine("cmd");
		cmd.addArgument("/c"); //C drive would be set
		cmd.addArgument("C:/Program Files/nodejs/node.exe");
		cmd.addArgument("C:/Users/bm071260/AppData/Local/Programs/Appium/resources/app/node_modules/appium/build/lib/appium.js");//Appium Path
		cmd.addArgument("--address", false);
		cmd.addArgument("127.0.0.1");
		cmd.addArgument("--port",false);
		cmd.addArgument("4723");
		cmd.addArgument("--full-reset", false);

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();  
		DefaultExecutor executor = new DefaultExecutor();  
		executor.setExitValue(1);  
		try {
			executor.execute(cmd, resultHandler);
			Thread.sleep(10000);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}


	}

	/**
	 *  this method stops the appium  server.
	 * @param os your machine OS (windows/linux/mac).
	 * @throws IOException Signals that an I/O exception of some sort has occurred.
	 * @throws ExecuteException An exception indicating that the executing a subprocesses failed.
	 */
	@AfterSuite
	public void stopAppiumServer() throws ExecuteException, IOException {
		CommandLine cmd = new CommandLine("cmd");
		cmd.addArgument("/c"); 
		cmd.addArgument("Task kill /F /IM node.exe");

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();  
		DefaultExecutor executor = new DefaultExecutor();  
		executor.setExitValue(1);  
		executor.execute(cmd, resultHandler);

	}

	@AfterMethod
	public void teardown(){
		System.out.println("Link to your job: https://saucelabs.com/jobs/" + this.getSessionID());
		webDriver.get().quit();
	}


}