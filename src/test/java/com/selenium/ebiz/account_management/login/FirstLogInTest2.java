package com.selenium.ebiz.account_management.login;

import com.appium.ebiz.baseclass.BaseClassV8_Mobile;
import com.selenium.ebiz.tools.PageManager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.List;

public class FirstLogInTest2 extends BaseClassV8_Mobile {

@Test
@Parameters({"platformName" ,"platformVersion", "deviceName"})

    public void login_verifyFirstLoginTest(String platformName, String platformVersion,String deviceName, Method methodName) throws Exception {
       
    this.createDriver(platformName, platformVersion, deviceName, methodName.getName());
    AppiumDriver<WebElement> driver = (AppiumDriver<WebElement>) this.getWebDriver();
    PageManager pageManager = new PageManager(driver);
   
    pageManager.loginPage().enterUserName("automation_id_26");
    pageManager.loginPage().enterPassword("password1");
    pageManager.loginPage().clickSigInButton();
    Thread.sleep(15000);
    
    pageManager.homePage().isDisplayedScanIcon();
    Log.info("Scan Icon is Displayed");
    
   
    pageManager.homePage().clickAddToCartButton();
    Thread.sleep(5000);
    
   
    pageManager.homePage().clickCartIcon();
    Thread.sleep(20000);
    
    
    Boolean isDisplayedshoppingCart = pageManager.shoppingCartPage().isDisplayedShoppingCartHeadingText();
    pageManager.shoppingCartPage().clickCheckoutButton();
    Thread.sleep(10000);
    
    pageManager.homePage().enterPONumber("1234");        
    
    pageManager.homePage().clickSubmitOrderButton();
    Thread.sleep(15000);
    
    Assert.assertTrue(isDisplayedshoppingCart);
       
    	
    	
    }

}
