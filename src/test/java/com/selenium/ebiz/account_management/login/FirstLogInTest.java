package com.selenium.ebiz.account_management.login;

import com.appium.ebiz.baseclass.BaseClassV8_Mobile;
import com.selenium.ebiz.tools.PageManager;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class FirstLogInTest extends BaseClassV8_Mobile {

@Test
@Parameters({"platformName" ,"platformVersion", "deviceName", "udid"})

    public void login_verifyFirstLoginTest(String platformName, String platformVersion,String deviceName, String udid) throws Exception {
       
    this.createDriver(platformName, platformVersion, deviceName, udid);
    AppiumDriver<WebElement> driver = (AppiumDriver<WebElement>) this.getWebDriver();
    PageManager pageManager = new PageManager(driver);
    
    pageManager.loginPage().signInToHDS(configread.getUserName(),configread.getPassword());
    Log.info("Log In To HDS");
    
    pageManager.homePage().addPartToCartAndSubmitOrder();
   // Log.info("Order Placed Successfully");
    	
    }

}
